# srrg_depth2laser - ROS node

## Description

This ROS node takes as input a depth image and extracts a laser scan from it.

## Installation

The package depends from

* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules) - _collection of cmake modules_
* [srrg_core](https://gitlab.com/srrg-software/srrg_core) - _core, i.e. low-level algorithms and utilities_
* [srrg_depth2laser](https://gitlab.com/srrg-software/srrg_depth2laser) - _depth2laser, i.e. package core library_

To install it, clone it in your workspace and run `catkin_make`.

## How to use it

To launch the node and the static TF , run

 `roslaunch srrg_depth2laser_ros test.launch `
 
If you want to try it on your mobile robot edit `test.launch` to set the laser parameters and the pose of the "virtual" laser.

## Examples
![alt text](http://i.imgur.com/a4774Lz.png "Laser scan overimposed xtion's pointcloud")

Laser scan overimposed xtion's pointcloud

![alt text](http://i.imgur.com/X3dYuHS.png "Any orientation is a good orientation")

Any orientation is a good orientation

Please cite the following paper to reference our work.

```
   @INPROCEEDINGS{NardiRobocup2018,
     author = {F. Nardi and M. T. Lazaro and L. Iocchi  and G. Grisetti},
     title = {Generation of laser-quality 2D navigation maps from RGB-D sensors},
     booktitle = {RoboCup Symposium 2018},
     month = {Jun 22},
     year = {2018},
     address = {Montreal, Canada}
   } 
```

